FROM openjdk:13-jdk-alpine
ADD /target/cicd-*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]