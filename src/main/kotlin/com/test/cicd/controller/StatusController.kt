package com.test.cicd.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class StatusController {

    @Value("\${application.name}")
    private lateinit var appName: String

    @Value("\${application.version}")
    private lateinit var appVersion: String

    @GetMapping("/status")
    fun version(): String {
        return "Service: $appName Version: $appVersion"
    }
}
